#! /usr/bin/env python3
# coding: utf-8

import pandas as pd


def load_data(path):

    # Names of the features:
    with open(path) as f:
        headers = f.readline().split(",")

    # Types of the features (except timestamp):
    dtypes = {'tweet_id': 'str', 'airline_sentiment': 'str', 'airline_sentiment_confidence': 'float',
              'negativereason': 'str', 'negativereason_confidence': 'float', 'airline': 'str',
              'airline_sentiment_gold': 'str', 'name': 'str', 'negativereason_gold': 'str', 'retweet_count': 'int',
              'text': 'str', 'tweet_coord': 'str', 'tweet_location': 'str', 'user_timezone': 'str'}

    # Timestamp data:
    parse_dates = ['tweet_created']

    # Import the data:
    return pd.read_csv(path, sep=',', header=None, skiprows=1, names=headers, dtype=dtypes, parse_dates=parse_dates)


def format_data(data):
    return data